﻿namespace SysIncident.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntitiesOfProject : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Departamento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 100, unicode: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .Index(t => t.Usuarios_Id);
            
            CreateTable(
                "dbo.Incidente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsuarioReportaId = c.Int(nullable: false),
                        UsuarioAsignadoId = c.Int(nullable: false),
                        PrioridadId = c.Int(nullable: false),
                        DepartamentoId = c.Int(nullable: false),
                        Titulo = c.String(maxLength: 200, unicode: false),
                        Descripcion = c.String(maxLength: 8000, unicode: false),
                        FechaCierre = c.DateTime(nullable: false),
                        ComentarioCierre = c.String(maxLength: 500, unicode: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departamento", t => t.DepartamentoId, cascadeDelete: true)
                .ForeignKey("dbo.Prioridad", t => t.PrioridadId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .Index(t => t.PrioridadId)
                .Index(t => t.DepartamentoId)
                .Index(t => t.Usuarios_Id);
            
            CreateTable(
                "dbo.Prioridad",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 50, unicode: false),
                        SlaId = c.Int(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sla", t => t.SlaId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .Index(t => t.SlaId)
                .Index(t => t.Usuarios_Id);
            
            CreateTable(
                "dbo.Sla",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        descripcion = c.String(maxLength: 200, unicode: false),
                        CantidadHoras = c.Int(nullable: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .Index(t => t.Usuarios_Id);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PuestoId = c.Int(nullable: false),
                        Nombre = c.String(maxLength: 100, unicode: false),
                        Apellido = c.String(maxLength: 100, unicode: false),
                        Cedula = c.String(maxLength: 11, unicode: false),
                        Correo = c.String(maxLength: 50, unicode: false),
                        Telefono = c.String(maxLength: 15, unicode: false),
                        FechaNacimiento = c.DateTime(nullable: false),
                        NombreUsuario = c.String(maxLength: 100, unicode: false),
                        Clave = c.String(maxLength: 500, unicode: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Puesto_Id = c.Int(),
                        Usuario_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Puesto", t => t.Puesto_Id)
                .ForeignKey("dbo.Puesto", t => t.PuestoId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_Id)
                .Index(t => t.PuestoId)
                .Index(t => t.Puesto_Id)
                .Index(t => t.Usuario_Id);
            
            CreateTable(
                "dbo.Historial_Incidente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidenteId = c.Int(nullable: false),
                        Comentario = c.String(maxLength: 500, unicode: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Incidente", t => t.IncidenteId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .Index(t => t.IncidenteId)
                .Index(t => t.Usuarios_Id);
            
            CreateTable(
                "dbo.Puesto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartamentoId = c.Int(nullable: false),
                        Nombre = c.String(maxLength: 100, unicode: false),
                        Estatus = c.String(maxLength: 2, unicode: false),
                        Borrado = c.Boolean(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        CreadoPor = c.Int(nullable: false),
                        ModificadoPor = c.Int(nullable: false),
                        Usuarios_Id = c.Int(),
                        Usuario_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departamento", t => t.DepartamentoId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.Usuarios_Id)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_Id)
                .Index(t => t.DepartamentoId)
                .Index(t => t.Usuarios_Id)
                .Index(t => t.Usuario_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Usuarios", "Usuario_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Sla", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Puesto", "Usuario_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "PuestoId", "dbo.Puesto");
            DropForeignKey("dbo.Puesto", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "Puesto_Id", "dbo.Puesto");
            DropForeignKey("dbo.Puesto", "DepartamentoId", "dbo.Departamento");
            DropForeignKey("dbo.Prioridad", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Incidente", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Historial_Incidente", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Historial_Incidente", "IncidenteId", "dbo.Incidente");
            DropForeignKey("dbo.Departamento", "Usuarios_Id", "dbo.Usuarios");
            DropForeignKey("dbo.Prioridad", "SlaId", "dbo.Sla");
            DropForeignKey("dbo.Incidente", "PrioridadId", "dbo.Prioridad");
            DropForeignKey("dbo.Incidente", "DepartamentoId", "dbo.Departamento");
            DropIndex("dbo.Puesto", new[] { "Usuario_Id" });
            DropIndex("dbo.Puesto", new[] { "Usuarios_Id" });
            DropIndex("dbo.Puesto", new[] { "DepartamentoId" });
            DropIndex("dbo.Historial_Incidente", new[] { "Usuarios_Id" });
            DropIndex("dbo.Historial_Incidente", new[] { "IncidenteId" });
            DropIndex("dbo.Usuarios", new[] { "Usuario_Id" });
            DropIndex("dbo.Usuarios", new[] { "Puesto_Id" });
            DropIndex("dbo.Usuarios", new[] { "PuestoId" });
            DropIndex("dbo.Sla", new[] { "Usuarios_Id" });
            DropIndex("dbo.Prioridad", new[] { "Usuarios_Id" });
            DropIndex("dbo.Prioridad", new[] { "SlaId" });
            DropIndex("dbo.Incidente", new[] { "Usuarios_Id" });
            DropIndex("dbo.Incidente", new[] { "DepartamentoId" });
            DropIndex("dbo.Incidente", new[] { "PrioridadId" });
            DropIndex("dbo.Departamento", new[] { "Usuarios_Id" });
            DropTable("dbo.Puesto");
            DropTable("dbo.Historial_Incidente");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Sla");
            DropTable("dbo.Prioridad");
            DropTable("dbo.Incidente");
            DropTable("dbo.Departamento");
        }
    }
}
