﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Incidente")]
    public class Incidente : Base
    {
        //[ForeignKey("Usuarios"), Column(Order = 1)]
        public int UsuarioReportaId {get; set;}
        //[ForeignKey("Usuarios"), Column(Order = 2)]
        public int UsuarioAsignadoId{ get; set;}
        public int PrioridadId { get; set; }
        public Prioridad Prioridad { get; set; }
        public int DepartamentoId { get; set; }
        public Departamento Departamento { get; set; }
        [MaxLength(200)]
        [Column(TypeName = "varchar")]
        public string Titulo { get; set; }
        [Column(TypeName = "varchar")]
        public string Descripcion { get; set; }
        public DateTime FechaCierre { get; set; }
        [MaxLength(500)]
        [Column(TypeName = "varchar")]
        public string ComentarioCierre { get; set; }

    }
}
