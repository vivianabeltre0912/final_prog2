﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Puesto")]
    public class Puesto:Base
    {
        public int DepartamentoId { get; set; }
        public Departamento Departamento { get; set; }
        [MaxLength(100)]
        [Column(TypeName = "varchar")]
        public string Nombre { get; set; }
        public List<Usuario> Users { get; set; }

    }
}
