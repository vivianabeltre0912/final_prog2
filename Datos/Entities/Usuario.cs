﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Usuarios")]
    public class Usuario:Base
    {
        public int PuestoId { get; set; }
        public Puesto Puesto { get; set; }
        [MaxLength(100)]
        [Column(TypeName = "varchar")]
        public string Nombre { get; set; }
        [MaxLength(100)]
        [Column(TypeName = "varchar")]
        public string Apellido { get; set; }
        [MaxLength(11)]
        [Column(TypeName = "varchar")]
        public string Cedula { get; set; }
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Correo { get; set; }
        [MaxLength(15)]
        [Column(TypeName = "varchar")]
        public string Telefono { get; set; }
        public DateTime FechaNacimiento { get; set; }
        [MaxLength(100)]
        [Column(TypeName = "varchar")]
        public string NombreUsuario { get; set; }
        [MaxLength(500)]
        [Column(TypeName = "varchar")]
        public string Clave { get; set; }
        public List<Departamento> Departamentos { get; set; }
        public List<Puesto> Puestos { get; set; }
        public List<HistorialIncidente> HistorialIncidentes { get; set; }
        public List<Incidente>  Incidentes { get; set; }
        public List<Prioridad> Prioridades { get; set; }
        public List<Sla> Slas  { get; set; }
        public new List<Usuario> Usuarios  { get; set; }


    }
}
