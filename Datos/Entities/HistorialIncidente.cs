﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Historial_Incidente")]
    public class HistorialIncidente:Base
    {
        public int IncidenteId { get; set; }
        public Incidente Incidente { get; set; }
        [MaxLength(500)]
        [Column(TypeName = "varchar")]
        public string Comentario { get; set; }
    }
}
