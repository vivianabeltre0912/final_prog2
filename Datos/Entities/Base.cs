﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    public class Base
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }
        [MaxLength(2)]
        [Column(TypeName = "varchar")]
        public string Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaModificacion { get; set; }
        //[ForeignKey("Usuarios")]
        //[Column(Order = 1)]
        public int CreadoPor { get; set; }
        //[ForeignKey("Usuarios")]
        //[Column(Order = 2)]
        public int ModificadoPor { get; set; }
        //[ForeignKey("CreadoPor, ModificadoPor ")]

        public Usuario Usuarios { get; set; }

    }
}
