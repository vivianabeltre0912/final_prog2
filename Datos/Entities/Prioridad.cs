﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Prioridad")]
    public class Prioridad:Base
    {
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Nombre { get; set; }
        public int SlaId { get; set; }
        public Sla Sla { get; set; }
        public List<Incidente> incidentes { get; set; }

    }
}
