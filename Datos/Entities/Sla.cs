﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Sla")]
    public class Sla:Base
    {
        [MaxLength(200)]
        [Column(TypeName = "varchar")]
        public string descripcion { get; set; }
        public int CantidadHoras { get; set; }
        public List<Prioridad> Prioridades { get; set; }

    }
}
