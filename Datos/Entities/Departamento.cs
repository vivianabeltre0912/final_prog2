﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SysIncident.Datos.Entities
{
    [Table("Departamento")]
    public class Departamento:Base
    {
        [MaxLength(100)]
        [Column(TypeName ="varchar")]
        public string Nombre { get; set; }
        public List<Puesto> Puestos { get; set; }
        public List<Incidente> incidentes { get; set; }

    }
}
