﻿using SysIncident.Datos.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysIncident.Datos.Context
{
    public class AppContext:DbContext
        
    {
        public AppContext()
            :base("name=mssql")
        {

        }

        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<HistorialIncidente> HistorialIncidentes { get; set; }
        public DbSet<Incidente> Incidentes { get; set; }
        public DbSet<Prioridad> Prioridades { get; set; }
        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<Sla> Slas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
    }
}
